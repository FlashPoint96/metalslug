package TheGame;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JLabel;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class main2 {
	static Window w = WindowSingleton.getWindowSingleton(main.f);

	static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	static crearpersonas p = new crearpersonas();
	static protagonista m;
	static ArrayList<Armas> Armas = new ArrayList<Armas>();
	static ArrayList<Armas2> Armas2 = new ArrayList<Armas2>();
	static mapa map = new mapa();
	static Timer timer = new Timer();

	static boolean derecha = true;
	static int minscroll = 50;
	static int maxscroll = 300;
	static boolean partida = false;

	public static void main(String[] args) throws InterruptedException {
		main.f.background = "gifs/plain-black-background.jpg";
		p.generador1();
		map.crearmapa1();
		System.out.println(mapa.Mapa.size());
		TimerTask task2 = new TimerTask() {

			@Override
			public void run() {
				disparosenemigo();
			}

		};
		timer.schedule(task2, 0, 5000);

		while (!partida) {
			dibujar();
			// System.out.println(sprites);
			// System.out.println(protagonista.getInstance().vida);
			sprites.addAll(Armas);
			sprites.addAll(Armas2);
			sprites.addAll(Drop.Drops);
			disparar();
			input();
			colisiones();
			partida = acabapartida();
			if (acabapartida()) {
				agregarpuntuacion();
				main2.w.close();
			}
			main.f.draw(sprites);
			sprites.clear();
			Thread.sleep(33);
		}
	}

	private static void agregarpuntuacion() {
		Scanner sc = new Scanner(System.in);
		// TODO Auto-generated method stub
		Text a = new Text("G", 250, 30, 300, 80, "HAS GANADO");
		a.textColor = 0xff0000;
		sprites.add(a);
		Text f = new Text("G", 250, 75, 300, 125, "Di un nombre para guardar tu puntuacion");
		sprites.add(f);
		f.textColor = 0xff0000;
		main.f.draw(sprites);
		PruebaBoton.main(null);
	}

	/**
	 * Esta funcion es para ver si ha acabado la partida.
	 * 
	 * @return
	 */
	private static boolean acabapartida() {
		// TODO Auto-generated method stub
		if (protagonista.getInstance().y1 >= 300 || mapa.Destructible.get(0).vida <= 0) {
			protagonista.getInstance().vivo = false;
			protagonista.borrar();
			return true;
		} else if (mapa.Destructible.get(0).x1 >= 2400) {
			return true;
		}
		return false;
	}

	/**
	 * Crea los disparos de los enemigos.
	 */
	private static void disparosenemigo() {
		for (int i = 0; i < crearpersonas.enemigos.size(); i++) {
			if (crearpersonas.enemigos.get(i).vivo && !crearpersonas.enemigos.get(i).mira
					&& !crearpersonas.enemigos.get(i).name.equals("DisparaArriba")) {
				Armas2.add(enemigo.dispararright(crearpersonas.enemigos.get(i)));
			} else if (crearpersonas.enemigos.get(i).vivo && crearpersonas.enemigos.get(i).mira
					&& !crearpersonas.enemigos.get(i).name.equals("DisparaArriba")) {
				Armas2.add(enemigo.dispararleft(crearpersonas.enemigos.get(i)));
			}
			if (crearpersonas.enemigos.get(i).name.equals("DisparaArriba") && crearpersonas.enemigos.get(i).vivo) {
				Armas2.add(enemigo.disparaarriba(crearpersonas.enemigos.get(i)));
			}
		}
	}

	/**
	 * Esta funcion sirve para dibujar.
	 */
	private static void dibujar() {
		m = protagonista.getInstance("Marco", 0, 120, 60, 180, "gifs/MarcoStanding.gif", 20, true, arma.prota);
		m = protagonista.getInstance();
		m.setConstantForce(0, 0.2);
		String v = String.valueOf(protagonista.getInstance().vida);
		String f = String.valueOf(protagonista.getInstance().getArma().actualbalas);

		Text vida = new Text("punt", 0, 0, 20, 20, "Vida " + v);
		Text balas = new Text("punt", 600, 0, 620, 20, "Balas " + f);
		vida.textColor = 0xFFFFFF;
		balas.textColor = 0xFFFFFF;
		sprites.add(vida);
		sprites.add(balas);
		sprites.add(m);
		sprites.addAll(mapa.Mapa);
		sprites.addAll(mapa.Destructible);
		sprites.addAll(crearpersonas.enemigos);
		main.f.draw(sprites);
	}

	/**
	 * Esta funcion es para disparar
	 */
	private static void disparar() {
		if (w.getPressedKeys().contains('g') && protagonista.getInstance().vivo) {
			if (arma.prota.actualbalas > 0) {
				arma.prota.actualbalas--;
				arma.prota.recarga = 0;
				if (derecha) {
					Armas.add(m.disparar());
				} else {
					Armas.add(m.dispararizquierda());
				}
			} else {
				Text b = new Text("punt", 150, 50, 200, 150, "NO TIENES BALAS PULSA R PARA RECARGAR");
				b.textColor = 0xff0000;
				sprites.add(b);
			}
		}

	}

	/**
	 * Comprueba las colisiones
	 * 
	 * @throws InterruptedException
	 */
	private static void colisiones() throws InterruptedException {
		// TODO Auto-generated method stub
		for (Armas armas : Armas) {
			armas.collision();
		}
		for (Armas armas : Armas) {
			armas.collisionMap();
		}
		for (Armas2 armas2 : Armas2) {
			armas2.collision();
		}
		if (m.isGrounded(main.f)) {
			m.ensuelo = true;
		}
		for (Drop drop : Drop.Drops) {
			drop.collision();
		}
		map.collision();
		mapa.mover(mapa.Destructible.get(0));

	}

	/**
	 * Esta funcion es el input por teclado.
	 */
	private static void input() {
		// TODO Auto-generated method stub
		if (w.getPressedKeys().contains('a') | w.getPressedKeys().contains('A')) {
			m.moveLeft();
			m.changeImage("gifs/MarcoWalkLeft.gif");
			derecha = false;
			if (m.x1 < minscroll) {
				main.f.scroll(5, 0);
				minscroll -= 5;
				maxscroll -= 5;
			}
		} else if (w.getPressedKeys().contains('d') | w.getPressedKeys().contains('D')) {
			m.moveRight();
			m.changeImage("gifs/MarcoWalk.gif");
			derecha = true;
			if (m.x1 > maxscroll) {
				main.f.scroll(-5, 0);
				minscroll += 5;
				maxscroll += 5;
			}
		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			m.doNotMove();
		}
		if (w.getPressedKeys().contains(' ')) {
			m.jump();
		}
		if (w.getPressedKeys().contains('k')) {
			System.out.println(m.x1 + " " + m.y1 + " " + m.x2 + " " + m.y2);
		}
		if (w.getPressedKeys().contains('r')) {
			protagonista.getInstance().recargar();
		}
	}

}
