package TheGame;

import java.util.ArrayList;

import Core.Sprite;

public class mapa {
	static ArrayList<Sprite> Mapa = new ArrayList<Sprite>();
	static ArrayList<TDestruc> Destructible = new ArrayList<TDestruc>();

	public mapa() {
		// TODO Auto-generated constructor stub

	}
	/**
	 * Esta funcion crea el mapa del nivel 1.
	 */
	public void crearmapa() {
		Terreno tierra = new Terreno("Floor", 0, 300, 2500, 350, "gifs/piedra.PNG");
		Mapa.add(tierra);
		Terreno tierra1 = new Terreno("Tierras", 472, 260, 530, 300, "gifs/piedra.PNG");
		Mapa.add(tierra1);
		Terreno tierra2 = new Terreno("Tierras", 530, 260, 650, 300, "gifs/piedra.PNG");
		Mapa.add(tierra2);
		Terreno tierra3 = new Terreno("Tierras", 650, 250, 700, 300, "gifs/piedra.PNG");
		Mapa.add(tierra3);
		Terreno tierra4 = new Terreno("Tierras", 530, 65, 580, 100, "gifs/piedra.PNG");
		Mapa.add(tierra4);
		Terreno tierra5 = new Terreno("Tierras", 940, 260, 1050, 300, "gifs/piedra.PNG");
		Mapa.add(tierra5);
		Terreno tierra6 = new Terreno("Tierras", 1100, 200, 1300, 230, "gifs/piedra.PNG");
		Mapa.add(tierra6);
		Terreno tierra7 = new Terreno("Tierras", 1350, 150, 1400, 180, "gifs/piedra.PNG");
		Mapa.add(tierra7);
		Terreno tierra8 = new Terreno("Tierras", 940, 150, 1000, 180, "gifs/piedra.PNG");
		Mapa.add(tierra8);
		Terreno tierra9 = new Terreno("Tierras", 1450, 180, 1800, 210, "gifs/piedra.PNG");
		Mapa.add(tierra9);
		Terreno tierra10 = new Terreno("Tierras", 0, 240, 50, 300, "gifs/piedra.PNG");
		Mapa.add(tierra10);

		TDestruc barill = new TDestruc("Barril", 400, 260, 450, 300, "gifs/Enemy/TopEnemyShot.png", 5);
		Destructible.add(barill);

		Terreno FINL1 = new Terreno("FINL1", 2100, 240, 2200, 300, "gifs/FINL1.gif");
		Mapa.add(FINL1);
	}
	/**
	 * Esta funcion crea el mapa del nivel 2.
	 */
	public void crearmapa1() {
		Terreno tierra = new Terreno("Floor", 0, 300, 2500, 350, "gifs/health.PNG");
		Mapa.add(tierra);
		Terreno tierra1= new Terreno("Floor", 500, 240, 560, 300, "gifs/piedra.PNG");
		Mapa.add(tierra1);		
		Terreno tierra3= new Terreno("Floor", 1440, 240, 1500, 300, "gifs/piedra.PNG");
		Mapa.add(tierra3);
		Terreno tierra4= new Terreno("Floor", 1500, 180, 1600, 220, "gifs/piedra.PNG");
		Mapa.add(tierra4);
		Terreno tierr5= new Terreno("Floor", 1600, 240, 1800, 260, "gifs/piedra.PNG");
		Mapa.add(tierr5);
		Terreno tierrd1 = new Terreno("Floor", 560, 240, 600, 300, "gifs/piedra.PNG");
		Mapa.add(tierrd1);

		
		TDestruc heli = new TDestruc("helo", 0, 60, 100, 120, "gifs/FINL1.gif",1);
		Destructible.add(heli);
		TDestruc tierra2= new TDestruc("Floor", 670, 100, 720, 300, "gifs/piedra.PNG",1);
		Destructible.add(tierra2);	
		TDestruc destu1= new TDestruc("Floor", 1000, 240, 1070, 300, "gifs/piedra.PNG",1);
		Destructible.add(destu1);	
		
	

	}

	/**
	 * Esta funcion comprueba si el protagonista ha colisionado con el helicoptero en el nivel 1 y pasarlo al nivel 2.
	 * @throws InterruptedException
	 */
	public void collision() throws InterruptedException {
		// TODO Auto-generated method stub
		for (int i = 0; i < mapa.Mapa.size(); i++) {
			if (protagonista.getInstance().collidesWith(mapa.Mapa.get(i))) {
				if (mapa.Mapa.get(i).name.equals("FINL1")) {
					System.out.println("GOING TO MAPA 2 LETS GO");

					crearpersonas.enemigos.clear();
					mapa.Mapa.clear();
					mapa.Destructible.clear();
					Drop.Drops.clear();
					main.Armas.clear();
					;
					main.Armas2.clear();
					protagonista.getInstance().x1 = 0;
					protagonista.getInstance().y1 = 120;
					protagonista.getInstance().x2 = 60;
					protagonista.getInstance().y2 = 180;
					main.f.resetScroll();
					main2.main(null);
				}
			}
		}
	}
	
	public static void mover(Sprite otro) {
		otro.x1+=3;
		otro.x2+=3d;
	}

}
