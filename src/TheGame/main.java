package TheGame;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.security.auth.callback.TextOutputCallback;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class main {

	static Field f = new Field();

	static Window w = WindowSingleton.getWindowSingleton(f);

	static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	static crearpersonas p = new crearpersonas();
	static protagonista m;
	static ArrayList<Armas> Armas = new ArrayList<Armas>();
	static ArrayList<Armas2> Armas2 = new ArrayList<Armas2>();
	static mapa map = new mapa();
	static Timer timer = new Timer();

	static boolean derecha = true;
	static int minscroll = 50;
	static int maxscroll = 300;
	static boolean partida = true;

	public static void main(String[] args) throws InterruptedException, IOException {
		f.background = "gifs/plain-black-background.jpg";
		p.generador();
		map.crearmapa();
		TimerTask task2 = new TimerTask() {

			@Override
			public void run() {
				disparosenemigo();
			}

		};
		timer.schedule(task2, 0, 5000);

		while (partida) {
			dibujar();
			// System.out.println(sprites.size());
			// System.out.println(protagonista.getInstance().vida);
			sprites.addAll(Drop.Drops);
			sprites.addAll(Armas);
			sprites.addAll(Armas2);
			disparar();
			input();
			colisiones();
			f.draw(sprites);
			sprites.clear();
			Thread.sleep(33);
		}
	}

	/**
	 * Crea los disparos de los enemigos.
	 */
	private static void disparosenemigo() {
		for (int i = 0; i < crearpersonas.enemigos.size(); i++) {
			if (crearpersonas.enemigos.get(i).vivo && !crearpersonas.enemigos.get(i).mira
					&& !crearpersonas.enemigos.get(i).name.equals("DisparaArriba")) {
				Armas2.add(enemigo.dispararright(crearpersonas.enemigos.get(i)));
			} else if (crearpersonas.enemigos.get(i).vivo && crearpersonas.enemigos.get(i).mira
					&& !crearpersonas.enemigos.get(i).name.equals("DisparaArriba")) {
				Armas2.add(enemigo.dispararleft(crearpersonas.enemigos.get(i)));
			}
		}

	}

	/**
	 * Esta funcion la llamo para dibujar el mapa.
	 */
	private static void dibujar() {
		m = protagonista.getInstance("Marco", 220, 220, 280, 280, "gifs/MarcoStanding.gif", 20, true, arma.prota);
		String v = String.valueOf(protagonista.getInstance().vida);
		String f = String.valueOf(protagonista.getInstance().getArma().actualbalas);

		Text vida = new Text("punt", 0, 0, 20, 20, "Vida " + v);
		Text balas = new Text("punt", 600, 0, 620, 20, "Balas " + f);

		vida.textColor = 0xFFFFFF;
		balas.textColor = 0xFFFFFF;
		sprites.add(vida);
		sprites.add(balas);
		m.setConstantForce(0, 0.2);
		sprites.add(m);
		sprites.addAll(mapa.Mapa);
		sprites.addAll(mapa.Destructible);
		sprites.addAll(crearpersonas.enemigos);
		// f.draw(sprites);
	}

	/**
	 * Esta es la funcion para disparar de Marco.
	 */
	private static void disparar() {
		int x = f.getCurrentMouseX();
		if (w.getPressedKeys().contains('g') && protagonista.getInstance().vivo) {
			if (arma.prota.actualbalas > 0) {
				arma.prota.actualbalas--;
				arma.prota.recarga = 0;
				if (derecha) {
					Armas.add(m.disparar());
				} else {
					Armas.add(m.dispararizquierda());
				}
			} else {
				Text b = new Text("punt", 150, 50, 200, 150, "NO TIENES BALAS PULSA R PARA RECARGAR");
				b.textColor = 0xff0000;
				sprites.add(b);
			}
		}

	}

	/**
	 * Esta funcion comprueba la colisiones
	 * 
	 * @throws InterruptedException
	 * @throws IOException 
	 */
	private static void colisiones() throws InterruptedException, IOException {
		// TODO Auto-generated method stub
		for (Armas armas : Armas) {
			armas.collision();
		}
		for (Armas armas : Armas) {
			armas.collisionMap();
		}
		for (Armas2 armas2 : Armas2) {
			armas2.collision();
		}
		if (m.isGrounded(f)) {
			m.ensuelo = true;
		}
		for (Drop drop : Drop.Drops) {
			drop.collision();
		}
		enemigo.col();

		if (protagonista.getInstance().y1 >= 300) {
			protagonista.getInstance().vivo = false;
			protagonista.borrar();
		}
		map.collision();
	}

	/**
	 * Esta funcion es el input por teclado.
	 */
	private static void input() {
		// TODO Auto-generated method stub
		if (w.getPressedKeys().contains('a') | w.getPressedKeys().contains('A')) {
			m.moveLeft();
			m.changeImage("gifs/MarcoWalkLeft.gif");
			derecha = false;
			if (m.x1 < minscroll) {
				f.scroll(5, 0);
				minscroll -= 5;
				maxscroll -= 5;
			}
		} else if (w.getPressedKeys().contains('d') | w.getPressedKeys().contains('D')) {
			m.moveRight();
			m.changeImage("gifs/MarcoWalk.gif");
			derecha = true;
			if (m.x1 > maxscroll) {
				f.scroll(-5, 0);
				minscroll += 5;
				maxscroll += 5;
			}
		}
		if (!w.getPressedKeys().contains('d') && !w.getPressedKeys().contains('a')) {
			m.doNotMove();
		}
		if (w.getPressedKeys().contains(' ')) {
			m.jump();
		}
		if (w.getPressedKeys().contains('k')) {
			// main2.main(null);
			System.out.println(m.x1 + " " + m.y1 + " " + m.x2 + " " + m.y2);
			System.out.println("LA VIDA DEL PERSONAJE ES " + m.vida);
			System.out.println("LAS BALAS QUE TIENE " + m.arma.actualbalas);
			System.out.println("EL DA�O QUE HACE " + m.arma.damage);
		}
		if (w.getPressedKeys().contains('r')) {
			protagonista.getInstance().recargar();
		}
	}

}
