package TheGame;

import java.util.ArrayList;
import java.util.Random;

import Core.Sprite;

public class Drop extends Sprite {
	int ammo;
	int vida;
	static ArrayList<Drop> Drops = new ArrayList<Drop>();

	public Drop(String name, int x1, int y1, int x2, int y2, String path, int ammo, int vida) {
		super(name, x1, y1, x2, y2, path);
		this.ammo = ammo;
		this.vida = vida;
		this.physicBody = false;
	}
	/**
	 * Esta es una funcion que llama a otras funciones para crear un item.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public static void crearrandom(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		Random random = new Random();
		int porce = random.nextInt(101);
		if (porce < 33) {
			crearvida(x1, y1, x2, y2);
		} else if (porce >= 33 && porce < 66) {
			crear(x1, y1, x2, y2);
		} else if (porce >= 66 && porce < 99) {
			boostatk(x1, y1, x2, y2);
		}
	}
	/**
	 * Comprueba las colisiones de los drop con el protagonista.
	 */
	public void collision() {
		// TODO Auto-generated method stub
		if (this.collidesWith(protagonista.getInstance())) {
			this.delete();
			if (this.name.equals("Municion")) {
				if (protagonista.getInstance().getArma().actualbalas <= protagonista.getInstance().getArma().minbalas) {
					protagonista.getInstance().getArma().actualbalas += this.ammo;
					if (protagonista.getInstance().getArma().actualbalas >=protagonista.getInstance().getArma().minbalas) {
						int resta = protagonista.getInstance().getArma().actualbalas
								- protagonista.getInstance().getArma().minbalas;
						protagonista.getInstance().getArma().maxbalas += resta;
						protagonista.getInstance().getArma().actualbalas = protagonista.getInstance()
								.getArma().minbalas;
					}
				} else {
					System.out.println(protagonista.getInstance().getArma().actualbalas);
				}
			} else if (this.name.equals("Vida")) {
				protagonista.getInstance().vida += this.vida;
				protagonista.getInstance().changeImage("gifs/MarcoEat.gif");
			} else if (this.name.equals("BoostAtk")) {
				if (protagonista.getInstance().getArma().damage < 10) {
					protagonista.getInstance().getArma().damage = protagonista.getInstance().getArma().damage + 5;
				} else {
					System.out.println("MAX DMG REACH");
				}
			}
		}
	}
	/**
	 * Esta funcion crea el item que da balas al protagonista.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public static void crear(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		Random random = new Random();
		Drop d = new Drop("Municion", x1, y1, x2, y2, "gifs/explosion.gif", random.nextInt(5 + 1), 0);
		Drops.add(d);
	}
	/**
	 * Esta funcion crea el item que da vida al protagonista.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public static void crearvida(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		Drop d = new Drop("Vida", x1, y1, x2, y2, "gifs/food.png", 0, 20);
		Drops.add(d);
	}
	/**
	 * Esta funcion crea el item que da mas dano al protagonista.
	 * @param x1
	 * @param y1
	 * @param x2
	 * @param y2
	 */
	public static void boostatk(int x1, int y1, int x2, int y2) {
		// TODO Auto-generated method stub
		if (protagonista.getInstance().getArma().damage <= 10) {
			Drop d = new Drop("BoostAtk", x1, y1, x2, y2, "gifs/dmgboost.png", 0, 0);
			Drops.add(d);
		}
	}

}
