package TheGame;

import Core.Sprite;

public class persona extends Sprite {
	int vida;
	boolean vivo;

	public persona(String name, int x1, int y1, int x2, int y2, String path, int vida, boolean vivo) {
		super(name, x1, y1, x2, y2, path);
		this.vida = vida;
		this.vivo = vivo;
	}
	/**
	 * Mover a la derecha.
	 */
	public void moveRight() {
		this.setVelocity(2, velocity[1]);
	}
	/**
	 * Mover a la izquierda.
	 */
	public void moveLeft() {
		this.setVelocity(-2, velocity[1]);
	}
	/**
	 * No moverse.
	 */
	public void doNotMove() {
		this.setVelocity(0, velocity[1]);
	}

}
