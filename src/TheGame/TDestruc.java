package TheGame;


public class TDestruc extends Terreno {
	 int vida;

	public TDestruc(String name, int x1, int y1, int x2, int y2, String path, int vida) {
		super(name, x1, y1, x2, y2, path);
		this.vida = vida;
	}
	/**
	 * Devuelve vida del terreno destructible.
	 * @return
	 */
	public int getVida() {
		return vida;
	}

	/**
	 * Permite modiciar la vida del terreno destructible.
	 * @param vida
	 */
	public void setVida(int vida) {
		this.vida = vida;
	}

}
