package TheGame;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class PruebaBoton {

	PruebaBoton() {
		JFrame f = new JFrame("");
		JButton b = new JButton("Enviar");
		b.setBounds(300, 240, 100, 45);
		JLabel label = new JLabel();
		label.setText(
				"Pon un Nombre para guardar tu puntuación que es de " + protagonista.getInstance().getPuntuacion());
		label.setBounds(200, 130, 500, 100);
		JTextField textfield = new JTextField();
		textfield.setBounds(300, 200, 100, 30);
		JLabel label1 = new JLabel();
		label1.setText("HAS GANADO");
		label1.setBounds(300, 110, 200, 100);
		f.add(textfield);
		f.add(label);
		f.add(label1);
		f.add(b);
		f.setSize(700, 400);
		f.setLayout(null);
		f.setVisible(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		f.setLocation(dim.width / 2 - f.getSize().width / 2, dim.height / 2 - f.getSize().height / 2);
		// action listener
		b.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String nom = textfield.getText();
				try {
					Ordenar.nuevapuntuacion(nom);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				f.dispose();
			}

		});
	}

	public static void main(String[] args) {
		System.out.println(protagonista.getInstance().vivo);
		if (protagonista.getInstance().vivo) {
			new PruebaBoton();
		} else {
			DerrotaBoton();
		}
	}

	private static void DerrotaBoton() {
		// TODO Auto-generated method stub
		JFrame f = new JFrame("");
		JButton b = new JButton("Enviar");
		b.setBounds(300, 240, 100, 45);
		JLabel label = new JLabel();
		label.setText(
				"Pon un Nombre para guardar tu puntuación que es de " + protagonista.getInstance().getPuntuacion());
		label.setBounds(200, 130, 500, 100);
		JTextField textfield = new JTextField();
		textfield.setBounds(300, 200, 100, 30);
		JLabel label1 = new JLabel();
		label1.setText("HAS PERDIDO");
		label1.setBounds(300, 110, 200, 100);
		f.add(textfield);
		f.add(label);
		f.add(label1);
		f.add(b);
		f.setSize(700, 400);
		f.setLayout(null);
		f.setVisible(true);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();

		f.setLocation(dim.width / 2 - f.getSize().width / 2, dim.height / 2 - f.getSize().height / 2);
		// action listener
		b.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {

				String nom = textfield.getText();
				try {
					Ordenar.nuevapuntuacion(nom);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				f.dispose();
			}

		});
	}
}
