package TheGame;

import java.io.IOException;
import java.util.Scanner;

import Core.Sprite;

public class Armas2 extends Sprite {
	static Scanner sc = new Scanner(System.in);

	public Armas2(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		this.physicBody = true;
		this.trigger = true;
	}

	/**
	 * Mover a la izquierda a la bala.
	 */
	public void moveleft() {
		// TODO Auto-generated method stub
		this.setVelocity(-4, 0);
	}

	/**
	 * Mover a la derecha a la bala.
	 */
	public void moveright() {
		// TODO Auto-generated method stub
		this.setVelocity(4, 0);
	}

	/**
	 * Comprueba las colisiones de las balas
	 */
	public void collision() {
		// TODO Auto-generated method stub
		if (this.collidesWith(protagonista.getInstance())) {
			protagonista.getInstance().vida -= 3;
			if (protagonista.getInstance().vida <= 0) {
				protagonista.getInstance().vivo = false;
				protagonista.getInstance().delete();
				main.partida = false;
				main.w.close();

				derrota();

			}
			this.delete();
		}
		for (int i = 0; i < mapa.Destructible.size(); i++) {
			if (this.collidesWith(mapa.Destructible.get(i))) {
				this.delete();
				mapa.Destructible.get(i).vida--;
				if (mapa.Destructible.get(i).vida <= 0) {
					main2.partida = true;
					main2.w.close();
					derrota();

				}
			}
		}
	}

	private void derrota() {
		// TODO Auto-generated method stub
		
		PruebaBoton.main(null);
		/*
		 * System.out.println("HAS PERDIDO DESGRACIADO");
		 * System.out.println("Di un nombre para guardar tu puntuacion"); String nom =
		 * sc.nextLine(); if(protagonista.getInstance().getPuntuacion()==0) {
		 * System.out.println("Puntuación tan lamentable que no la guardo"); }else {
		 * Puntuacion.creararchivo(nom); }
		 */
	}

	/**
	 * Mover la bala arriba.
	 */
	public void movearriba() {
		// TODO Auto-generated method stub
		this.setVelocity(0, -4);
	}

}
