package TheGame;

public class protagonista extends persona implements recargar {

	private static protagonista instance = null;
	arma arma;
	boolean ensuelo = false;
	int puntuacion = 0;
	public int getPuntuacion() {
		return puntuacion;
	}

	public void setPuntuacion(int puntuacion) {
		this.puntuacion = puntuacion;
	}

	private protagonista(String name, int x1, int y1, int x2, int y2, String path, int vida, boolean vivo, arma arma) {
		super(name, x1, y1, x2, y2, path, vida, vivo);
		this.arma = arma;
	}

	public static protagonista getInstance(String name, int x1, int y1, int x2, int y2, String path, int vida,
			boolean vivo, arma arma) {
		if (instance == null) {
			instance = new protagonista(name, x1, y1, x2, y2, path, vida, vivo, arma);
		}
		return instance;
	}
	/**
	 * Esta funcion es para disparar para el protagonista.
	 * @return
	 */
	public Armas disparar() {
		// TODO Auto-generated method stub
		Armas m = new Armas("Derecha", x1 + 20, y1 + 25, x2 - 20, y2 - 25, 0, "gifs/disparoderecha.png");
		m.moveright();
		protagonista.getInstance(name, x1, y1, x2, y2, path, vida, ensuelo, arma)
				.changeImage("gifs/MarcoShotRight.gif");
		;
		return m;
	}
	/**
	 * Devuelve el arma.
	 * @return
	 */
	public arma getArma() {
		return arma;
	}
	/**
	 * Permite cambiar cosas del arma.
	 * @param arma
	 */
	public void setArma(arma arma) {
		this.arma = arma;
	}
	/**
	 * Devuelve el protagonista.
	 * @return
	 */
	public static protagonista getInstance() {
		return instance;
	}
	/**
	 * Permite disparar a la izquierda.
	 * @return
	 */
	public Armas dispararizquierda() {
		// TODO Auto-generated method stub
		Armas m = new Armas("Izquierda", x1 + 20, y1 + 25, x2 - 20, y2 - 25, 0, "gifs/disparoderecha.png");
		m.moveleft();
		protagonista.getInstance(name, x1, y1, x2, y2, path, vida, ensuelo, arma).changeImage("gifs/MarcoShotLeft.gif");
		;
		return m;
	}
	/**
	 * Funcion para saltar.
	 */
	public void jump() {
		if (ensuelo == true) {
			this.setForce(0, -1.2);
			ensuelo = false;
		}

	}
	/**
	 * Es la interficie que le permite recargar.
	 */
	@Override
	public void recargar() {
		// TODO Auto-generated method stub
		if (arma.actualbalas <= 30) {
			int sumaba = arma.minbalas - arma.actualbalas;
			if (sumaba >= arma.maxbalas) {
			} else {
				arma.actualbalas += sumaba;
				arma.maxbalas -= sumaba;
			}
		}
	}
	/**
	 * Borrar el protagonista.
	 */
	public static void borrar() {
		if (!protagonista.getInstance().vivo) {
			protagonista.getInstance().delete();
			WindowSingleton.w.close();
		}
	}

}
