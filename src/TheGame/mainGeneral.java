package TheGame;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Core.Field;
import Core.Sprite;
import Core.Window;

public class mainGeneral {
	static Field f = new Field();

	static Window w = WindowSingleton.getWindowSingleton(f);
	static ArrayList<Sprite> sprites = new ArrayList<Sprite>();
	static ArrayList<boton> botones = new ArrayList<boton>();
	static boolean juego = true;
	public static int y1 = 30;
	public static int y2 = 100;

	public static void main(String[] args) throws InterruptedException, IOException {
		f.background = "gifs/MENU.jpg";

		botonesinicio();
		while (juego) {
			colisiones();
			f.draw(sprites);
			Thread.sleep(33);
		}

	}

	private static void botonesinicio() {
		// TODO Auto-generated method stub
		botones.clear();
		sprites.clear();
		boton b = new boton("Empezar", 280, 20, 350, 100, 0, "gifs/boton-de-play.png");
		boton c = new boton("Niveles", 280, 120, 350, 200, 0, "gifs/lista.png");
		boton d = new boton("Cerrar", 280, 220, 350, 300, 0, "gifs/cerrar.png");
		boton f = new boton("Ranking", 20, 20, 100, 100, 0, "gifs/estrella.png");
		boton volver = new boton("Volver", 580, 20, 660, 100, 0, "gifs/espalda.png");
		botones.add(b);
		botones.add(c);
		botones.add(d);
		botones.add(f);
		botones.add(volver);
		sprites.addAll(botones);

	}

	private static void colisiones() throws InterruptedException, IOException {
		// TODO Auto-generated method stub

		int x = f.getCurrentMouseX();
		int y = f.getCurrentMouseY();
			for (int i = 0; i < botones.size(); i++) {
				if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Empezar")) {
					main.main(null);
				} else if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Niveles")) {
					botonesniveles();
				} else if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Cerrar")) {
					System.out.println("ADIOS");
					w.close();
					juego = false;
				}
				if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Volver")) {
					botonesinicio();
				}
				if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Nivel 1")) {
					main.main(null);
				} else if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Nivel 2")) {
					main2.main(null);
				}
				if (botones.get(i).collidesWithPoint(x, y) && botones.get(i).name.equals("Ranking")) {
					ranking();
				}
			}	
		
	}

	private static void ranking() throws IOException {
		// TODO Auto-generated method stub
		String cadena;
		botonesvol();
		Ordenar.main(null);
		Text F1 = new Text("punt", 280, 0, 350, 50, "HIGHSCORES");
		F1.textColor = 0x1A7CD4;
		FileReader f = new FileReader("Puntuaciones/Puntuaciones.txt");
		BufferedReader b = new BufferedReader(f);
		while ((cadena = b.readLine()) != null) {
			Text texto = new Text("punt", 280, y1, 350, y2, cadena + "\n");
			texto.textColor = 0xFF0000;
			y1+=20;
			y2+=20;
			sprites.add(texto);
			sprites.add(F1);
		}
		b.close();
		y1 = 30;
		y2 = 100;
	}



	private static void botonesvol() {
		// TODO Auto-generated method stub
		sprites.clear();
		botones.clear();
		boton volver = new boton("Volver", 580, 20, 660, 100, 0, "gifs/espalda.png");
		botones.add(volver);
		sprites.addAll(botones);
	}

	private static void botonesniveles() {
		// TODO Auto-generated method stub
		sprites.clear();
		botones.clear();
		boton volver = new boton("Volver", 580, 20, 660, 100, 0, "gifs/espalda.png");
		boton n1 = new boton("Nivel 1", 20, 20, 100, 100, 0, "gifs/uno.png");
		boton n2 = new boton("Nivel 2", 120, 20, 200, 100, 0, "gifs/dos.png");
		botones.add(volver);
		botones.add(n1);
		botones.add(n2);
		sprites.addAll(botones);
	}
}
