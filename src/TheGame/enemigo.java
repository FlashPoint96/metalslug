package TheGame;

import Core.Sprite;

public class enemigo extends persona {

	boolean mira;
	boolean ensuelo;

	public enemigo(String name, int x1, int y1, int x2, int y2, String path, int vida, boolean vivo, boolean mira,
			boolean ensuelo) {
		super(name, x1, y1, x2, y2, path, vida, vivo);
		this.mira = mira;
		this.ensuelo = ensuelo;
	}
	/**
	 * Esta funcion es para disparar a la derecha por lo crea una bala y la mueva a la derecha.
	 * @param otro
	 * @return
	 */
	public static Armas2 dispararright(Sprite otro) {
		// TODO Auto-generated method stub
		Armas2 m = new Armas2("Disparo", otro.x1 + 30, otro.y1 + 40, otro.x2 - 30, otro.y2 - 60, 0,
				"gifs/Enemy/LeftEnemyShot.png");
		m.moveright();
		return m;
	}
	/**
	 * Esta funcion es para disparar a la izquierda por lo crea una bala y la mueva a la izquierda.
	 * @param otro
	 * @return
	 */
	public static Armas2 dispararleft(Sprite otro) {
		// TODO Auto-generated method stub
		Armas2 m = new Armas2("Disparo", otro.x1 + 30, otro.y1 + 40, otro.x2 - 30, otro.y2 - 60, 0,
				"gifs/Enemy/RightEnemyShot.png");
		m.moveleft();
		return m;
	}
	/**
	 * Esta funcion es para saltar.
	 */
	public void jump() {
		if (ensuelo == true) {
			this.setForce(0, -1);
			ensuelo = false;
		}

	}
	/**
	 * Esta funcion comprueba si el enemigo ha colisionado con el suelo.
	 */
	public static void col() {
		// TODO Auto-generated method stub
		for (int i = 0; i < crearpersonas.enemigos.size(); i++) {
			if (crearpersonas.enemigos.get(i).isGrounded(main.f)) {
				crearpersonas.enemigos.get(i).ensuelo = true;
			}
		}
	}
	/**
	 * Esta funcion crea una bala y la mueve hacia arriba.
	 * @param otro
	 * @return
	 */
	public static Armas2 disparaarriba(Sprite otro) {
		// TODO Auto-generated method stub
		Armas2 m = new Armas2("Disparo", otro.x1 + 30, otro.y1 + 40, otro.x2 - 50, otro.y2 - 40, 0,
				"gifs/Enemy/TopEnemyShot.png");
		m.movearriba();
		return m;
	}

}
