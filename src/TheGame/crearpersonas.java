package TheGame;

import java.util.ArrayList;

public class crearpersonas {
	static ArrayList<enemigo> enemigos = new ArrayList<enemigo>();

	/**
	 * Esta funcion crea los enemigos del Nivel1 .
	 */
	public void generador() {
		enemigo e1 = new enemigo("EnemigoL2", 60, 190, 150, 300, "gifs/Enemy/LeftEnemy.gif", 10, true, false, true);
		enemigo e2 = new enemigo("EnemigoL1", 480, 150, 560, 260, "gifs/Enemy/RightEnemy.gif", 10, true, true, true);
		enemigo e3 = new enemigo("EnemigoL1", 940, 50, 1030, 160, "gifs/Enemy/LeftEnemy.gif", 10, true, false, true);
		enemigo e4 = new enemigo("EnemigoL1", 570, 150, 650, 260, "gifs/Enemy/RightEnemy.gif", 10, true, true, true);
		enemigo e5 = new enemigo("EnemigoL1", 660, 150, 740, 260, "gifs/Enemy/RightEnemy.gif", 10, true, true, true);
		enemigo e6 = new enemigo("EnemigoL1", 1700, 80, 1780, 190, "gifs/Enemy/RightEnemy.gif", 10, true, true, true);

		enemigos.add(e1);
		enemigos.add(e2);
		enemigos.add(e3);
		enemigos.add(e4);
		enemigos.add(e5);
		enemigos.add(e6);


	}
	/**
	 * Esta funcion crea los enemigos del Nivel 2.
	 */
	public void generador1() {
		enemigo e1 = new enemigo("DisparaArriba", 300, 190, 390, 300, "gifs/Enemy/LeftEnemy.gif", 10, true, true, true);
		enemigos.add(e1);
		enemigo e2 = new enemigo("DisparaArriba", 800, 190, 890, 300, "gifs/Enemy/RightEnemy.gif", 10, true, true,
				true);
		enemigos.add(e2);
		enemigo e3 = new enemigo("DisparaArriba", 1200, 190, 1290, 300, "gifs/Enemy/RightEnemy.gif", 10, true, true,
				true);
		enemigos.add(e3);
		enemigo e4 = new enemigo("DisparaArriba", 1500, 80, 1590, 190, "gifs/Enemy/RightEnemy.gif", 10, true, true,
				true);
		enemigos.add(e4);
		
		enemigo e5 = new enemigo("DisparaArriba", 1950, 190, 2040, 300, "gifs/Enemy/RightEnemy.gif", 10, true, true,
				true);
		enemigos.add(e5);
		enemigo e6 = new enemigo("DisparaArriba", 2050, 190, 2140, 300, "gifs/Enemy/RightEnemy.gif", 10, true, true,
				true);
		enemigos.add(e6);
		

	}

}
