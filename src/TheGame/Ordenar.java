package TheGame;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class Jugador {
	String name;

	int puntuacion;

	public Jugador(String name, int puntuacion) {
		this.name = name;

		this.puntuacion = puntuacion;
	}
}

class nameCompare implements Comparator<Jugador> {
	@Override
	public int compare(Jugador s1, Jugador s2) {
		return s1.name.compareTo(s2.name);
	}
}

class marksCompare implements Comparator<Jugador> {
	@Override
	public int compare(Jugador s1, Jugador s2) {
		return s2.puntuacion - s1.puntuacion;
	}
}

public class Ordenar {
	public static int linias;
	public static int juga10;

	public static void main(String[] args) throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader("Puntuaciones/Puntuaciones.txt"));

		ArrayList<Jugador> RecordJugador = new ArrayList<Jugador>();

		String currentLine = reader.readLine();

		while (currentLine != null) {
			String[] Jugado = currentLine.split(" ");

			String name = Jugado[0];

			int Puntuacion = Integer.valueOf(Jugado[1]);

			RecordJugador.add(new Jugador(name, Puntuacion));

			currentLine = reader.readLine();
		}

		Collections.sort(RecordJugador, new marksCompare());
		BufferedWriter writer = new BufferedWriter(new FileWriter("Puntuaciones/Puntuaciones.txt"));
		int punt = 0;
		for (Jugador jugador : RecordJugador) {
			writer.write(jugador.name);

			writer.write(" " + jugador.puntuacion);
			punt = jugador.puntuacion;
			writer.newLine();

		}

		reader.close();

		writer.close();
		juga10 = punt;
	}

	public static void contarlinias() throws IOException {
		// TODO Auto-generated method stub
		BufferedReader reader = new BufferedReader(new FileReader("Puntuaciones/Puntuaciones.txt"));
		linias = 0;
		while (reader.readLine() != null) {
			linias++;
		}
		reader.close();

	}

	public static void borrarultimo() throws IOException {

		BufferedReader reader = new BufferedReader(new FileReader("Puntuaciones/Puntuaciones.txt"));

		ArrayList<Jugador> RecordJugador = new ArrayList<Jugador>();

		String currentLine = reader.readLine();

		while (currentLine != null) {
			String[] Jugado = currentLine.split(" ");

			String name = Jugado[0];

			int Puntuacion = Integer.valueOf(Jugado[1]);

			RecordJugador.add(new Jugador(name, Puntuacion));

			currentLine = reader.readLine();
		}

		Collections.sort(RecordJugador, new marksCompare());

		BufferedWriter writer = new BufferedWriter(new FileWriter("Puntuaciones/Puntuaciones.txt"));
		if (RecordJugador.size() > 0) {
			RecordJugador.remove(RecordJugador.size() - 1);
		}
		for (Jugador jugador : RecordJugador) {
			writer.write(jugador.name);

			writer.write(" " + jugador.puntuacion);

			writer.newLine();
		}

		reader.close();

		writer.close();
	}

	public static void nuevapuntuacion(String nom) throws IOException {
		Ordenar.main(null);
		contarlinias();

		if (linias <= 10) {
			if (protagonista.getInstance().getPuntuacion() > juga10) {
				borrarultimo();
				Puntuacion.creararchivo(nom);
			} else {
				System.out.println("PUNTACION PEOR QUE LA ULTIMA");
			}
		} else {
			Puntuacion.creararchivo(nom);
		}
	}
}
