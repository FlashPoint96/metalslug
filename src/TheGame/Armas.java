package TheGame;

import java.util.Random;

import Core.Sprite;

public class Armas extends Sprite {
	Random random = new Random();

	public Armas(String name, int x1, int y1, int x2, int y2, double angle, String path) {
		super(name, x1, y1, x2, y2, angle, path);
		this.physicBody = true;
		this.trigger = true;
	}
	/**
	 * Mover a la derecha a la bala.
	 */
	public void moveright() {
		// TODO Auto-generated method stub
		this.setVelocity(4, 0);
	}
	/**
	 * Mover a la izquierda a la bala.
	 */
	public void moveleft() {
		// TODO Auto-generated method stub
		this.setVelocity(-4, 0);
	}
	
	/**
	 * Comprueba las colisiones de las balas con los enemigos.
	 */
	public void collision() {
		// TODO Auto-generated method stub
		for (int i = 0; i < crearpersonas.enemigos.size(); i++) {
			if (this.collidesWith(crearpersonas.enemigos.get(i))) {
				crearpersonas.enemigos.get(i).vida -= arma.prota.damage;
				this.delete();
				if (crearpersonas.enemigos.get(i).vida <= 5 && !crearpersonas.enemigos.get(i).mira) {
					crearpersonas.enemigos.get(i).moveLeft();
				} else if (crearpersonas.enemigos.get(i).vida <= 5 && crearpersonas.enemigos.get(i).mira) {
					crearpersonas.enemigos.get(i).moveRight();
				}
				if (crearpersonas.enemigos.get(i).vida <= 0) {
					protagonista.getInstance().setPuntuacion(protagonista.getInstance().getPuntuacion()+100);
					crearpersonas.enemigos.get(i).delete();
					crearpersonas.enemigos.get(i).vivo = false;
					if (random.nextBoolean()) {
						Drop.crearrandom(crearpersonas.enemigos.get(i).x1, crearpersonas.enemigos.get(i).y1,
								crearpersonas.enemigos.get(i).x2, crearpersonas.enemigos.get(i).y2);
					} else {
					}
				}
			}
		}
	}

	/**
	 * Comprueba las colisiones de las balas con el mapa.
	 */
	public void collisionMap() {
		// TODO Auto-generated method stub
		for (int i = 0; i < mapa.Mapa.size(); i++) {
			if (this.collidesWith(mapa.Mapa.get(i))) {
				this.delete();
			}
		}
		for (int i = 0; i < mapa.Destructible.size(); i++) {
			if (this.collidesWith(mapa.Destructible.get(i))) {
				this.delete();
				mapa.Destructible.get(i).setVida(mapa.Destructible.get(i).getVida() - 1);
				if (mapa.Destructible.get(i).getVida() <= 0) {
					mapa.Destructible.get(i).delete();
					Drop.crearrandom(mapa.Destructible.get(i).x1, mapa.Destructible.get(i).y1,
							mapa.Destructible.get(i).x2, mapa.Destructible.get(i).y2);
				}
			}
		}
	}

}
